
const http = require('http'),
      fs = require('fs'),
      async = require('async'),
      pushover = require('pushover'),
      repos = pushover('./')

const auth = function(req, cb){
  var authenticated = false
  fs.readDir('./keys', function(err, files){
    async.each(files, function(file, done){
      fs.readFile('./keys/' + file, function(err, files){
          if(err){
            done(err)
          } else {
            //Check the key here
          }
      })
    }, function(err){
      cb(err, authenticated)
    })
  })
}

repos.on('push', function(push){
  console.log("**************push ****************************", new Date())
  console.log('push', push)
  console.log("************************************************")
  push.accept()

})

repos.on('fetch', function(fetch){
  console.log('fetch', fetch)
  fetch.accept()
})

var server = http.createServer(function(req, res){
  console.log('*****************req****************', new Date())
  console.log(req)
  console.log("************************************")
  repos.handle(req, res)
})

server.listen(process.env.PORT || 7000)
console.log("alive", repos)
